package ddc

import org.scalatest.FunSuite


class ShoppingCartServiceTest extends FunSuite {

  val shoppingCartService = new ShoppingCartService


  test("calculateTotalCost computes total no offers found") {
    assert(shoppingCartService.calculateTotalCost("orange", "pear") == BigDecimal(0.25)  )
  }

  test("calculateTotalCost buy one get one free on apples") {
    assert(shoppingCartService.calculateTotalCost("apple", "apple", "orange", "pear") == BigDecimal(0.85)  )
  }

  test("calculateTotalCost 3 for price of 2 on orange") {
    val orangesBought = 4
    val applesBought = 2
    val expectedTotal:BigDecimal = (0.25 * (orangesBought - 1)) + (0.60 * (applesBought - 1))

    assert(shoppingCartService.calculateTotalCost("orange","orange","orange","orange",
      "apple", "apple", "pear") == expectedTotal)
  }

  test("calculateTotalCost 3 for price of 2 on oranges x 2, and 2 for 1 apples x 2") {
    val orangesBought = 6
    val applesBought = 4
    val expectedTotal:BigDecimal = (0.25 * (orangesBought - 2)) + (0.60 * (applesBought - 2))

    assert(shoppingCartService.calculateTotalCost("orange","orange","orange","orange","orange","orange",
      "apple", "apple","apple", "apple", "pear") == expectedTotal)
  }


}
