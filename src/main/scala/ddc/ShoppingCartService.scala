package ddc

import scala.collection.mutable

/**
  * Shopping Cart costing service
  */
class ShoppingCartService {

  val costMap:Map[String,BigDecimal] = Map("apple" -> BigDecimal(0.60), "orange" -> BigDecimal(0.25))


  def calculateTotalCost(items: String*): BigDecimal = {

    var instancesBought = collection.mutable.Map[String, Int]()
    var totalCost: BigDecimal = BigDecimal(0)

    // Count Instances bought
    items.foreach((item: String) =>
      instancesBought.get(item) match {
        case Some(i: Int) => instancesBought.update(item, i + 1)
        case None => instancesBought += (item -> 1)
      }
    )


    // Apply Offers
    val offers = new OfferRepository().getOffers()

    offers.foreach((offer: Offer) => {
      println(s"offer is $offer")
      instancesBought.get(offer.itemName) match {
        case Some(quantityBought: Int) => {
          var instancesOfOffer = quantityBought / offer.discountAtQuantity
          var quantityDiscount = instancesOfOffer * offer.discountQuantityBy
          println(s"Bought some on offer. Qty:$quantityBought quantityDiscount:$quantityDiscount")
          instancesBought.update(offer.itemName ,  quantityBought + quantityDiscount)
          println(s"Revised Qty:$quantityBought")
        }
        case None =>
      }

    })

    val getPrice: (String) => BigDecimal = (itemName) => costMap.get(itemName:String) match {
      case Some(i) => i
      case None => 0 }

    // Price the Quantities now that offers have been applied
    // totalCost = instancesBought.values.foldLeft(0) { _ + (_ * getPrice(?) ) }
    for ((keyItemName, valueQty) <- instancesBought) {
      val price:BigDecimal = getPrice(keyItemName)
      totalCost = totalCost + (price * valueQty)
      println(s"Item:$keyItemName price:$price, totalCost:$totalCost")
    }

    totalCost;
  }

}



//    val getPrice = (itemName: String) => BigDecimal =  costMap.get(itemName:String) match {