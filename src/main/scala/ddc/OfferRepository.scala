package ddc

/**
  */
class OfferRepository {

   def getOffers(): List[Offer] = {
    List(new Offer("apple",2, -1), new Offer("orange", 3, -1))
  }

}
